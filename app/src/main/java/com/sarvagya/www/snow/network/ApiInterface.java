package com.sarvagya.www.snow.network;

import com.sarvagya.www.snow.models.News;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface ApiInterface {

    @GET
    Observable<Response<News>> getNews(@Url String url);
}
