package com.sarvagya.www.snow.injection.components;

import android.support.v4.app.Fragment;

import com.sarvagya.www.snow.activities.BookmarksActivtiy;
import com.sarvagya.www.snow.activities.DetailedNewsActivity;
import com.sarvagya.www.snow.activities.MainActivity;
import com.sarvagya.www.snow.fragments.BitcoinNewsFragment;
import com.sarvagya.www.snow.fragments.MainFragment;
import com.sarvagya.www.snow.injection.modules.ApiInterfaceModule;
import com.sarvagya.www.snow.injection.modules.PicassoModule;
import com.sarvagya.www.snow.injection.scopes.PerActivityScope;

import dagger.Component;

@PerActivityScope
@Component(modules = {ApiInterfaceModule.class, PicassoModule.class})
public interface MyAppComponent {

    void injectMainFragment(MainFragment mainFragment);

    void injectBitcoinFragment(BitcoinNewsFragment bitcoinNewsFragment);

    void injectDetailedNewsActivity(DetailedNewsActivity detailedNewsActivity);

    void injectBookmarksActivity(BookmarksActivtiy bookmarksActivtiy);
}
