package com.sarvagya.www.snow.network;

public class Api {

    public static final String BASE_URL = "https://newsapi.org/v2/";

    public static final String topHeadlineUrl = "top-headlines?country=us&category=business&apiKey=c6009da9b9b0432689cd21bb36a7771d";

    public static final String appleArticlesUrl = "everything?q=apple&from=2018-06-10&to=2018-06-10&sortBy=popularity&apiKey=c6009da9b9b0432689cd21bb36a7771d";

    public static final String bitcoinNewsUrl = "everything?q=bitcoin&sortBy=publishedAt&apiKey=c6009da9b9b0432689cd21bb36a7771d";

    public static final String wallStreetUrl = "everything?domains=wsj.com&apiKey=c6009da9b9b0432689cd21bb36a7771d";

}
