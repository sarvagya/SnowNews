package com.sarvagya.www.snow.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.widget.TextView;

import com.sarvagya.www.snow.R;

public class TypeFaceUtil {

    public static void philosopher_bold(Context context, TextView tv){
        Typeface typeface = ResourcesCompat.getFont(context, R.font.philosopher_bold);
        tv.setTypeface(typeface);
    }

    public static void raleway_medium(Context context, TextView tv){
        Typeface typeface = ResourcesCompat.getFont(context, R.font.raleway_medium);
        tv.setTypeface(typeface);
    }

    public static void raleway_bold(Context context, TextView tv){
        Typeface typeface = ResourcesCompat.getFont(context, R.font.raleway_bold);
        tv.setTypeface(typeface);
    }

}
