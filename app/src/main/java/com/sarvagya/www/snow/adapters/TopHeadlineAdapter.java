package com.sarvagya.www.snow.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.activities.DetailedNewsActivity;
import com.sarvagya.www.snow.models.Articles;
import com.sarvagya.www.snow.models.News;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TopHeadlineAdapter extends RecyclerView.Adapter<TopHeadlineAdapter.CustomViewHolder> {

    private ArrayList<Articles> articlesList;
    private Context context;
    private Picasso picasso;
    private LayoutInflater inflater;

    public TopHeadlineAdapter(Context context, ArrayList<Articles> articlesList, Picasso picasso){
        this.context = context;
        this.articlesList = articlesList;
        this.picasso = picasso;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.top_headline_list_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        Articles articles = articlesList.get(position);
        Log.d("checkNews", articles.getPublishedAt() + "\n " + articles.getSource());

        holder.tvHeadLine.setText(articles.getTitle());

        if (!articles.getUrlToImage().isEmpty()) {
            picasso.load(articles.getUrlToImage())
                    .centerCrop()
                    .fit()
                    .placeholder(R.drawable.img_default)
                    .into(holder.imgNews);
        } else {
            picasso.load(R.drawable.img_default)
                    .centerCrop()
                    .fit()
                    .into(holder.imgNews);

        }

    }

    @Override
    public int getItemCount() {
        return articlesList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgNews;
        private TextView tvHeadLine;

        public CustomViewHolder(View itemView) {
            super(itemView);

            imgNews = itemView.findViewById(R.id.imgNews);
            tvHeadLine = itemView.findViewById(R.id.tvHeadLine);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Articles articles = articlesList.get(getAdapterPosition());

                    Intent intent = new Intent(context, DetailedNewsActivity.class);
                    intent.putExtra("title", articles.getTitle());
                    intent.putExtra("desc", articles.getDescription());
                    intent.putExtra("image", articles.getUrlToImage());
                    intent.putExtra("source", articles.getSource().getName());
                    intent.putExtra("author", articles.getAuthor());
                    intent.putExtra("publishedAt", articles.getPublishedAt());

                    context.startActivity(intent);
                }
            });
        }

    }
}
