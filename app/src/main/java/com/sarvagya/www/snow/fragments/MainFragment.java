package com.sarvagya.www.snow.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.Utils.NetworkUtil;

import com.sarvagya.www.snow.adapters.TopHeadlineAdapter;
import com.sarvagya.www.snow.injection.components.DaggerMyAppComponent;
import com.sarvagya.www.snow.injection.components.MyAppComponent;
import com.sarvagya.www.snow.injection.modules.ContextModule;
import com.sarvagya.www.snow.models.Articles;
import com.sarvagya.www.snow.models.News;
import com.sarvagya.www.snow.network.Api;
import com.sarvagya.www.snow.network.ApiInterface;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    ProgressBar pb;

    private RecyclerView businessRecyclerView, appleRecyclerView, wallStreetRecyclerView;

    @Inject
    ApiInterface apiInterface;

    @Inject
    Picasso picasso;

    private TextView tvBusinessHeadlines, tvApple, tvWallstreet;

    CompositeSubscription compositeSubscription;


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        initVars(view);
        compositeSubscription = new CompositeSubscription();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        businessRecyclerView = view.findViewById(R.id.businessRecyclerView);
        businessRecyclerView.setLayoutManager(linearLayoutManager);

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        appleRecyclerView = view.findViewById(R.id.appleRecyclerView);
        appleRecyclerView.setLayoutManager(linearLayoutManager2);

        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        wallStreetRecyclerView = view.findViewById(R.id.wallStreetRecyclerView);
        wallStreetRecyclerView.setLayoutManager(linearLayoutManager3);

        MyAppComponent component = DaggerMyAppComponent.builder()
                .contextModule(new ContextModule(getActivity()))
                .build();

        //injecting apiInterface and picasso
        component.injectMainFragment(MainFragment.this);

        if (NetworkUtil.isNetworkAvailable(getActivity())) {
            fetchTopHeadlineNews();
            fetchAppleArticles();
            fetchWallStreetArticles();
        } else {
        }

        return view;
    }

    private void initVars(View view) {
        tvBusinessHeadlines = view.findViewById(R.id.tvBusinessHeadlines);
        tvApple = view.findViewById(R.id.tvApple);
        tvWallstreet = view.findViewById(R.id.tvWallstreet);
        pb = view.findViewById(R.id.pb);


    }


    private void fetchWallStreetArticles() {

        Subscription subscription = apiInterface.getNews(Api.wallStreetUrl)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Response<News> newsResponse) {

                        News news = newsResponse.body();
                        ArrayList<Articles> articlesList = new ArrayList<>(Arrays.asList(news.getArticles()));


                        TopHeadlineAdapter adapter = new TopHeadlineAdapter(getContext(), articlesList, picasso);
                        wallStreetRecyclerView.setAdapter(adapter);
                    }
                });


    }

    private void fetchAppleArticles() {

        Subscription subscription = apiInterface.getNews(Api.wallStreetUrl)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Response<News> newsResponse) {

                        News news = newsResponse.body();
                        ArrayList<Articles> articlesList = new ArrayList<>(Arrays.asList(news.getArticles()));

                        TopHeadlineAdapter adapter = new TopHeadlineAdapter(getContext(), articlesList, picasso);
                        appleRecyclerView.setAdapter(adapter);
                    }
                });
    }


    private void fetchTopHeadlineNews() {

        pb.setVisibility(View.VISIBLE);

        Subscription subscription = apiInterface.getNews(Api.topHeadlineUrl)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Response<News> newsResponse) {
                        pb.setVisibility(View.GONE);

                        tvBusinessHeadlines.setVisibility(View.VISIBLE);
                        tvApple.setVisibility(View.VISIBLE);
                        tvWallstreet.setVisibility(View.VISIBLE);

                        News news = newsResponse.body();
                        ArrayList<Articles> articlesList = new ArrayList<>(Arrays.asList(news.getArticles()));

                        TopHeadlineAdapter adapter = new TopHeadlineAdapter(getContext(),articlesList, picasso);
                        businessRecyclerView.setAdapter(adapter);

                    }
                });


        compositeSubscription.add(subscription);

    }


}
