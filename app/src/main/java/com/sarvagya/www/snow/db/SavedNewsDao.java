package com.sarvagya.www.snow.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface SavedNewsDao {

    @Insert
    void insertNews(SavedNews savedNews);


    @Query("SELECT * FROM SavedNews")
    List<SavedNews> getAllSavedNews();

    @Delete
    void deleteBookmarkedNews(SavedNews savedNews);
}
