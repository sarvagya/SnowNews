package com.sarvagya.www.snow.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.db.SavedNews;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.CustomViewHolder> {

    private Context context;
    private List<SavedNews> savedNewsArrayList;
    private LayoutInflater inflater;
    private Picasso picasso;

    public BookmarksAdapter(Context context, List<SavedNews> savedNewsArrayList, Picasso picasso){
        this.context = context;
        this.savedNewsArrayList = savedNewsArrayList;
        this.picasso = picasso;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.other_news_list_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

        SavedNews savedNews = savedNewsArrayList.get(position);

        if (savedNews.getImageUrl() != null) {
            picasso.load(savedNews.getImageUrl())
                    .centerCrop()
                    .fit()
                    .into(holder.imgNews);
        } else {
            picasso.load(R.drawable.img_default)
                    .centerCrop()
                    .fit()
                    .into(holder.imgNews);

        }

        holder.tvTitle.setText(savedNews.getTitle());
        holder.tvSource.setText(savedNews.getSource());
        holder.tvPublishedAt.setText(savedNews.getPublishedAt());

    }

    @Override
    public int getItemCount() {
        return savedNewsArrayList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgNews;
        private TextView tvTitle, tvSource, tvPublishedAt;

        public CustomViewHolder(View itemView) {
            super(itemView);

            imgNews = itemView.findViewById(R.id.imgNews);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSource = itemView.findViewById(R.id.tvSource);
            tvPublishedAt = itemView.findViewById(R.id.tvPublishedAt);
        }
    }
}
