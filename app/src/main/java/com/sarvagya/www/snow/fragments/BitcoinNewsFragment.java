package com.sarvagya.www.snow.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.Utils.NetworkUtil;
import com.sarvagya.www.snow.activities.MainActivity;
import com.sarvagya.www.snow.adapters.BitcoinAdapter;
import com.sarvagya.www.snow.adapters.TopHeadlineAdapter;
import com.sarvagya.www.snow.injection.components.DaggerMyAppComponent;
import com.sarvagya.www.snow.injection.components.MyAppComponent;
import com.sarvagya.www.snow.injection.modules.ContextModule;
import com.sarvagya.www.snow.models.Articles;
import com.sarvagya.www.snow.models.News;
import com.sarvagya.www.snow.network.Api;
import com.sarvagya.www.snow.network.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class BitcoinNewsFragment extends Fragment {
    ProgressBar pb;

    private RecyclerView bitcoinRecyclerView;

    private CompositeSubscription compositeSubscription;

    @Inject
    ApiInterface apiInterface;

    @Inject
    Picasso picasso;

    public BitcoinNewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bitcoin_news, container, false);

        pb = view.findViewById(R.id.pb);

        compositeSubscription = new CompositeSubscription();

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        bitcoinRecyclerView = view.findViewById(R.id.bitcoinRecyclerView);
        bitcoinRecyclerView.setLayoutManager(linearLayoutManager1);


        MyAppComponent component = DaggerMyAppComponent.builder()
                .contextModule(new ContextModule(getActivity()))
                .build();

        //injecting apiInterface and picasso
        component.injectBitcoinFragment(BitcoinNewsFragment.this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //checking the network connection
        if (NetworkUtil.isNetworkAvailable(getContext())) {
            fetchBitcoinNews();
        } else {
            Toast.makeText(getContext(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
        }


    }

    private void fetchBitcoinNews() {
        pb.setVisibility(View.VISIBLE);

        Subscription subscription = apiInterface.getNews(Api.bitcoinNewsUrl)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Response<News> newsResponse) {
                        pb.setVisibility(View.GONE);

                        News news = newsResponse.body();

                        ArrayList<Articles> otherNewsArticles = new ArrayList<>(Arrays.asList(news.getArticles()));

                        BitcoinAdapter adapter = new BitcoinAdapter(getContext(), otherNewsArticles, picasso);
                        bitcoinRecyclerView.setAdapter(adapter);
                    }
                });

        compositeSubscription.add(subscription);

    }
}
