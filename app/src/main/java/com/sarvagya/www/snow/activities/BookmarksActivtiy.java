package com.sarvagya.www.snow.activities;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.adapters.BookmarksAdapter;
import com.sarvagya.www.snow.db.Database;
import com.sarvagya.www.snow.db.SavedNews;
import com.sarvagya.www.snow.injection.components.DaggerMyAppComponent;
import com.sarvagya.www.snow.injection.components.MyAppComponent;
import com.sarvagya.www.snow.injection.modules.ContextModule;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BookmarksActivtiy extends AppCompatActivity {

    private RecyclerView rvBookmarks;
    private List<SavedNews> savedNewsArrayList;
    private Database db;

    @Inject
    Picasso picasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks_activtiy);

        initVars();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Bookmarks");
        }

        MyAppComponent component = DaggerMyAppComponent.builder()
                .contextModule(new ContextModule(getApplicationContext()))
                .build();

        component.injectBookmarksActivity(this);

        getArrayListFromDb();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvBookmarks.setLayoutManager(linearLayoutManager);
        rvBookmarks.setHasFixedSize(true);

        BookmarksAdapter adapter = new BookmarksAdapter(this, savedNewsArrayList, picasso);
        rvBookmarks.setAdapter(adapter);


    }

    private void getArrayListFromDb() {
        db = Room.databaseBuilder(this, Database.class, "database-name")
                .allowMainThreadQueries()
                .build();

        savedNewsArrayList = db.savedNewsDao().getAllSavedNews();
    }

    private void initVars() {
        rvBookmarks = findViewById(R.id.rvBookmarks);
        savedNewsArrayList = new ArrayList<>();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
