package com.sarvagya.www.snow.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.sarvagya.www.snow.R;

import com.sarvagya.www.snow.fragments.BitcoinNewsFragment;
import com.sarvagya.www.snow.fragments.MainFragment;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Snow News");

            SectionAdapter adapter = new SectionAdapter(getSupportFragmentManager());

            ViewPager pager = (ViewPager) findViewById(R.id.pager);
            pager.setAdapter(adapter);

            TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
            tabs.setupWithViewPager(pager);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.bookmarked){
            Intent intent = new Intent(MainActivity.this, BookmarksActivtiy.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);

    }

    private class SectionAdapter extends FragmentPagerAdapter {
        public SectionAdapter(FragmentManager fm){
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            if(position == 0){
                frag = new MainFragment();
            }else if (position == 1){
                frag = new BitcoinNewsFragment();
            }

            return frag;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if(position == 0){
                return "All News";
            }else if (position == 1){
                return "Bitcoin News";
            }

            return super.getPageTitle(position);
        }
    }}


