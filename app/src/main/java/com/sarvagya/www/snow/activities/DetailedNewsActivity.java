package com.sarvagya.www.snow.activities;

import android.arch.persistence.room.Room;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.Utils.TypeFaceUtil;
import com.sarvagya.www.snow.db.Database;
import com.sarvagya.www.snow.db.SavedNews;
import com.sarvagya.www.snow.injection.components.DaggerMyAppComponent;
import com.sarvagya.www.snow.injection.components.MyAppComponent;
import com.sarvagya.www.snow.injection.modules.ContextModule;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class DetailedNewsActivity extends AppCompatActivity {

    private TextView tvTitle, tvDesc, tvSource, tvPublishedAt, tvAuthor;
    private ImageView imageView;

    @Inject
    Picasso picasso;

    Database db;

    String title, desc, source, author, publishedAt, imageUrl;

    boolean pressed = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_news);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Snow News");
        }

        initVars();

        title = getIntent().getStringExtra("title");
        desc = getIntent().getStringExtra("desc");
        imageUrl = getIntent().getStringExtra("image");
        source = getIntent().getStringExtra("source");
        publishedAt = getIntent().getStringExtra("publishedAt");
        author = getIntent().getStringExtra("author");

        tvTitle.setText(title);
        TypeFaceUtil.philosopher_bold(this, tvTitle);
        tvDesc.setText(desc);
        tvSource.setText(source);
        tvAuthor.setText(author);
        tvPublishedAt.setText(publishedAt.substring(0, 10));
        TypeFaceUtil.philosopher_bold(this, tvPublishedAt);

        MyAppComponent component = DaggerMyAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

        component.injectDetailedNewsActivity(this);

        picasso.load(imageUrl).fit().centerCrop().into(imageView);


        db = Room.databaseBuilder(this, Database.class, "database-name")
                .allowMainThreadQueries()
                .build();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();

        } else if (item.getItemId() == R.id.bookmark) {

            SavedNews savedNews = new SavedNews();

            insertNewsIntoDb(savedNews);

            Drawable myDrawable = getResources().getDrawable(R.drawable.ic_bookmars_pressed);
            item.setIcon(myDrawable);

        }
        return super.onOptionsItemSelected(item);
    }

    private void insertNewsIntoDb(SavedNews savedNews) {

        savedNews.setTitle(title);
        savedNews.setDesc(desc);
        savedNews.setSource(source);
        savedNews.setPublishedAt(publishedAt);
        savedNews.setImageUrl(imageUrl);
        savedNews.setAuthor(author);

        db.savedNewsDao().insertNews(savedNews);

    }

    private void initVars() {

        tvTitle = findViewById(R.id.tvTitle);
        tvDesc = findViewById(R.id.tvDesc);
        tvSource = findViewById(R.id.tvSource);
        imageView = findViewById(R.id.imageView);
        tvPublishedAt = findViewById(R.id.tvPublishedAt);
        tvAuthor = findViewById(R.id.tvAuthor);
    }
}
