package com.sarvagya.www.snow.db;

import android.arch.persistence.room.RoomDatabase;

@android.arch.persistence.room.Database(entities = {SavedNews.class}, version = 1)
public abstract class Database extends RoomDatabase {

    public abstract SavedNewsDao savedNewsDao();
}
