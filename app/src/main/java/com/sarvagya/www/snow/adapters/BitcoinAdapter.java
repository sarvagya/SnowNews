package com.sarvagya.www.snow.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvagya.www.snow.R;
import com.sarvagya.www.snow.activities.DetailedNewsActivity;
import com.sarvagya.www.snow.models.Articles;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BitcoinAdapter extends RecyclerView.Adapter<BitcoinAdapter.CustomViewHolder> {

    private ArrayList<Articles> newsList;
    private Context context;
    private Picasso picasso;
    private LayoutInflater inflater;

    public BitcoinAdapter(Context context, ArrayList<Articles> newsList, Picasso picasso) {
        this.context = context;
        this.newsList = newsList;
        this.picasso = picasso;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.other_news_list_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        Articles articles = newsList.get(position);

        //Log.d("")articles.getUrlToImage().length();

        if (!articles.getUrlToImage().isEmpty()) {
            picasso.load(articles.getUrlToImage())
                    .centerCrop()
                    .fit()
                    .placeholder(R.drawable.img_default)
                    .into(holder.imgNews);
        } else {
            picasso.load(R.drawable.img_default)
                    .centerCrop()
                    .fit()
                    .into(holder.imgNews);

        }

        holder.tvTitle.setText(articles.getTitle());
        holder.tvSource.setText(articles.getSource().getName());
        holder.tvPublishedAt.setText(articles.getPublishedAt().substring(0, 10));
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgNews;
        private TextView tvTitle, tvSource, tvPublishedAt;

        public CustomViewHolder(View itemView) {
            super(itemView);

            imgNews = itemView.findViewById(R.id.imgNews);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSource = itemView.findViewById(R.id.tvSource);
            tvPublishedAt = itemView.findViewById(R.id.tvPublishedAt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Articles articles = newsList.get(getAdapterPosition());

                    Intent intent = new Intent(context, DetailedNewsActivity.class);
                    intent.putExtra("title", articles.getTitle());
                    intent.putExtra("desc", articles.getDescription());
                    intent.putExtra("image", articles.getUrlToImage());
                    intent.putExtra("source", articles.getSource().getName());
                    intent.putExtra("author", articles.getAuthor());
                    intent.putExtra("publishedAt", articles.getPublishedAt());

                    context.startActivity(intent);
                }
            });

        }
    }
}
